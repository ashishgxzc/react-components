import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            phoneNumber: '',
            code: '',
            type: 'login'
        }
    }

    componentDidMount() {

    }

    handleChange = fieldName => val => {
        console.log("value", fieldName, value);
        let value = val;
        if (val.hasOwnProperty('target')) {
            value = val.target.value;
        }
        this.setState({
            [fieldName]: value
        });
    }

    getOTP = () => {
        const { phoneNumber, code } = this.state;
        const data = { phoneNumber };
        fetch('https://consumer-dev.neo91.com/api/v1/auth/send-otp', {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Mobile-App': true,
                Platform: 'android',
                'X-Client-Version': '19',
                'X-User-Id': '123',
                'X-device-unique-id': '123123',
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify(data)

        })
            .then(response => response.json())
            .then(data => {
                console.log("Data", data);
                if (data.type === 'success') {
                    this.setState({
                        type: 'otp'
                    });
                }
            })
    }

    verifyOTP = () => {
        const { phoneNumber, code } = this.state;
        const data = { phoneNumber, otp: code, };
        fetch('https://consumer-dev.neo91.com/api/v1/auth/verify-otp', {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-Mobile-App': true,
                Platform: 'android',
                'X-Client-Version': '19',
                'X-User-Id': '123',
                'X-device-unique-id': '123123',
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => {
                console.log("Data", data);
                if (data.type === 'success') {
                    this.setState({
                        type: 'success'
                    });
                }
            })
    }



    render() {
        const { type, phoneNumber, code } = this.state;
        return (
            <div style={{
                padding: "20px",
            }}>
                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignContent: 'center',
                }}>
                    <h4>Sign In</h4>
                    <h4>Sign Up</h4>
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                justifyContent: 'center',
                alignContent: 'center',
            }}>
                {
                    (type === 'login')
                        ?
                        <div>
                            <p>Login</p>
                            <input onChange={this.handleChange('phoneNumber')} style={{ height: 40, width: 200, margin: 10 }} maxLength={10} />
                            <button style={{
                                height: 48,
                                width: 200,
                                margin: 10
                            }}
                                onClick={this.getOTP}

                            > Submit </button>
                        </div>
                        : null

                }
                {
                    (type === 'otp')
                        ? <div>
                            <p>Enter OTP</p>
                            <input onChange={this.handleChange('code')} style={{ height: 40, width: 200, margin: 10 }} maxLength={10} />
                            <button style={{
                                height: 50,
                                width: 200
                            }}
                                onClick={this.verifyOTP}
                            > Submit </button>
                        </div> : null
                }
                {
                    (type === 'register')
                        ? <div>
                            <p>Enter</p>
                            <input onChange={this.handleChange('code')} style={{ height: 40, width: 200, margin: 10 }} maxLength={10} />
                            <button style={{
                                height: 50,
                                width: 200
                            }}
                                onClick={this.verifyOTP}
                            > Submit </button>
                        </div> : null
                }
                {
                    (type === 'success')
                        ? <div>
                            <h4>You are now logged in</h4>
                        </div> : null
                }

            </div>
            </div>
        )
    }
}

ReactDOM.render(
    React.createElement(App, {}, null),
    document.getElementById('react-target')
);
